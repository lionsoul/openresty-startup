-- 
-- common parameters checking
-- 
function is_numeric(...) 
    local arg = {...};

    local num;
    for _,v in ipairs(arg) do
        num = tonumber(v);
        if ( nil == num ) then
            return false;
        end
    end

    return true;
end


local args = ngx.req.get_uri_args();
if not args.a or not args.b or not is_numeric(args.a, args.b) then
    ngx.exit(ngx.HTTP_BAD_REQUEST);
    return;
end
